/**
 * @jest-environment jsdom
 */

import {BrowserStorageManager} from '../utils/BrowserStorageManager';

describe('Browser Storage Manager', () => {
  let bsm: BrowserStorageManager = new BrowserStorageManager('session', 'test');
  test('It should Create an Instance of BrowserStorageManager', () => {
    expect(bsm.constructor.name).toEqual('BrowserStorageManager');
  });

  test('It should add key-value pair to the store and get it too from the store', () => {
    bsm = bsm.add('keys', 'key1', 'value1');
    const prop = bsm.getProp('keys', 'key1', null);
    expect(prop).toEqual('value1');
  });

  test('It should be able to tell if key exists already in store', () => {
    const theKeyKeysExists = bsm.hasKey('keys');
    expect(theKeyKeysExists).toBeTruthy();
  });

  test('It should be able to tell if key exists already in `namespace/path/prop` ', () => {
    const theKeyKey1Exists = bsm.has('keys', 'key1');
    expect(theKeyKey1Exists).toBeTruthy();
  });

});
