/**
 * @jest-environment jsdom
 */

import {MockService as MS} from '../services/MockDataService';
import {DateFilter} from '../utils/Utility';

describe('Date Filter', () => {
  test('It should match Date to Swiss Format Date', () => {
    const dateNow = new Date();
    const swissDateNow = DateFilter.swissDate(dateNow, 'en')
      .split(' at ')[0]
      .replace(/^([a-z]+\. )(\d+)( [a-z]+\. )(\d+)/gi, '$2/$4')
      .replace(/^0/, '');
    const result = `${dateNow.getDate()}/${dateNow.getFullYear()}`;
    expect(swissDateNow).toEqual(result);
  });

  test('It should be an Integer less between 10 & 20', () => {
    expect(MS.generateRandomInt(10, 20)).toBeLessThan(20);
  });

  test('It should generate a Decimal Number...', () => {
    expect(typeof MS.generateDecimal(10, 20)).toBe('number');
  });
});
