/**
 * @jest-environment jsdom
 */

import {NotificationTriggerSlot as NTS} from '../components/NotificationTriggerSlot';
import {
  DateFilter,
  camelFy,
  dispatchCustomEvent,
  filterAnimationClasses,
  generateQuadRandomHash,
  getDefaultToolTipConfig,
  isLowerCase,
  isUpperCase,
  playAudio,
  snakeFy,
  ucFirst,
  ucWords,
} from '../utils/Utility';

describe('Notification-Trigger Slot Component', () => {
  const infoTrigger:NTS = new NTS("info", 'info');
  const warnTrigger:NTS = new NTS("warning", "warning");
  const errorTrigger:NTS = new NTS("error", "error");

  test('Info Trigger is wrapped in a DIV Element...', () => {
    expect(infoTrigger.create().tagName.toUpperCase()).toEqual('DIV');
  });

  test('Warn Trigger is wrapped in a DIV Element...', () => {
    expect(warnTrigger.create().tagName.toUpperCase()).toEqual('DIV');
  });

  test('Error Trigger is wrapped in a DIV Element...', () => {
    expect(errorTrigger.create().tagName.toUpperCase()).toEqual('DIV');
  });
});
