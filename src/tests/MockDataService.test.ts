/**
 * @jest-environment jsdom
 */
import {MockService as MS} from '../services/MockDataService';

describe('Mock Data Service', () => {
  test('It should return Integer', () => {
    const testArray: string[] = [
      'one',
      'two',
      'three',
      'four',
      'five',
      'six',
      'seven',
      'eight',
    ];
    expect(MS.generateRandomInt(10, 20)).toBeLessThan(20);
    expect(typeof MS.generateDecimal(10, 20)).toBe('number');
    expect(typeof MS.generateRandomStatus()).toBe('string');
    expect(MS.shuffleArray(testArray)).toHaveLength(8);
    expect(MS.getBaseTextLatin()).toContain('Lorem');
    expect(MS.getBaseTextEnglish()).toContain('Night');
    expect(MS.getBaseNumbers()).toMatch(/[0-9]+/);
    expect(typeof MS.generateArbitraryNumber(5)).toBe('number');
    expect(['info', 'warning', 'error']).toContain(MS.generateRandomType());
    expect(MS.pullRandomElementOffArray(testArray)).toMatch(/[a-z]{3,5}/);
    expect(MS.generateArbitraryNumber(5).toString()).toMatch(/\d{4,}/);
    expect(MS.generateArbitraryNumber(5)).toBeGreaterThan(999);
    expect(MS.getNumericTextSegments(5, true).join('')).toMatch(/\d{4,}/);
    expect(MS.generateText(5).split(' ')).toHaveLength(5);
    expect(MS.generateLatinText(5).split(' ')).toHaveLength(5);
  });
});
