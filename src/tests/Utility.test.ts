/**
 * @jest-environment jsdom
 */

import {MockService as MS} from '../services/MockDataService';
import {
  DateFilter,
  camelFy,
  dispatchCustomEvent,
  filterAnimationClasses,
  generateQuadRandomHash,
  getDefaultToolTipConfig,
  isLowerCase,
  isUpperCase,
  playAudio,
  snakeFy,
  ucFirst,
  ucWords,
} from '../utils/Utility';

describe('General Utility', () => {
  test('It should Match Swiss Date against UTC Date', () => {
    const dateNow = new Date();
    const swissDateNow = DateFilter.swissDate(dateNow, 'en')
      .split(' at ')[0]
      .replace(/^([a-z]+\. )(\d+)( [a-z]+\. )(\d+)/gi, '$2/$4')
      .replace(/^0/, '');
    const result = `${dateNow.getDate()}/${dateNow.getFullYear()}`;
    expect(swissDateNow).toEqual(result);
  });

  test('It should generate a random Number between 10 and 20 (exclusive)', () => {
    expect(MS.generateRandomInt(10, 20)).toBeLessThan(20);
  });

  test('It should generate a Decimal Number which is of Type `number`', () => {
    expect(typeof MS.generateDecimal(10, 20)).toBe('number');
  });

  test('It should turn first Letter to Upper case', () => {
    expect(ucFirst('beam')).toEqual('Beam');
  });

  test('It should turn first Letters of each word to Upper case', () => {
    expect(ucWords('beam is a tech company in paris.')).toEqual(
      'Beam Is A Tech Company In Paris.',
    );
  });

  test('It should return true if given character is in Lower-Case.', () => {
    expect(isLowerCase('b')).toBeTruthy();
  });

  test('It should return true if given character is in Upper-Case.', () => {
    expect(isUpperCase('B')).toBeTruthy();
  });

  test('It should return convert any string to Camel-Case.', () => {
    expect(camelFy('string with spaces And Upper Cases')).toEqual(
      'stringWithSpacesAndUpperCases',
    );
  });

  test('It should return convert any string to Snake/Kebab-Case.', () => {
    expect(snakeFy('string with spaces And Upper Cases')).toEqual(
      'string_with_spaces_and_upper_cases',
    );
  });

  test('It should play Audio and return...', async () => {
    const audio: HTMLAudioElement = await playAudio("assets/audio/Basso.m4a");
    expect(audio.tagName.toUpperCase()).toEqual('AUDIO');
  });

  test('Default Tooltip Config must contain Hello as part of it content entry.', () => {
    const ttConfig: any = getDefaultToolTipConfig("Hello ToolTip!");
    expect(ttConfig.content.includes('Hello')).toBeTruthy();
  });
});
