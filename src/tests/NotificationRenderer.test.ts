/**
 * @jest-environment jsdom
 */
import {MessageInterface} from '../interfaces/MessageInterface';
import {Message} from '../models/Message';
import {NotificationRenderer} from '../components/NotificationRenderer';

describe('Notification Renderer', () => {
  test('It should create NotificationsRenderer Object', () => {
    // USE THE STATIC METHOD: `Message.generateArbitraryMessage` TO GENERATE TEST-MESSAGE OBJECT
    const msg: MessageInterface = new Message(Message.generateArbitraryMessage());
    const notificationRenderer: NotificationRenderer = new NotificationRenderer(msg);
    expect(notificationRenderer.constructor.name).toBe('NotificationRenderer');
  });

  test('It should be able to render Notifications from any part of the App', () => {
    /*
    TO RENDER NOTIFICATION FROM ANY PART OF THE APP,
    JUST CREATE A NEW `Message` OBJECT THAT CONFORMS TO THE `MessageInterface`.
    THEN PASS IT AS THE SOLE ARGUMENT TO `NotificationRenderer` AND SIMPLY CALL
    THE `render()` METHOD ON THE INSTANCE - BY DEFAULT THIS RETURNS THE TOP-LEVEL
    DOM ELEMENT CONTAINING THE ENTIRE HTML FOR THE NOTIFICATION...
    PLEASE NOTE THAT THE RENDER METHOD TAKES INTO CONSIDERATION THE OUTPUT MEDIUM
    CONFIGURED IN THE SETTINGS...
     */
    // USE THE STATIC METHOD: `Message.generateArbitraryMessage` TO GENERATE TEST-MESSAGE OBJECT
    const msg: MessageInterface = new Message(Message.generateArbitraryMessage());
    const nRenderer: NotificationRenderer = new NotificationRenderer(msg);
    const htmlRender: HTMLElement = nRenderer.render();
    expect(htmlRender.tagName.toUpperCase()).toEqual('DIV');

  });
});
