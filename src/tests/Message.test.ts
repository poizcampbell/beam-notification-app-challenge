/**
 * @jest-environment jsdom
 */

import {MessageInterface, MessageOperationsInterface} from '../interfaces/MessageInterface';
import {Message} from '../models/Message';
import {MessageType} from '../interfaces/AppTypes';

describe('Message (Model)', () => {
  test('It should create a new Message Instance', () => {
    const msg: Message = new Message(Message.generateArbitraryMessage(false))
    expect(msg.constructor.name).toEqual('Message');
  });
  test('Newly generated7created Notification/Message should have status set to `unread`', () => {
    const msg: Message = new Message(Message.generateArbitraryMessage(false))
    expect(msg.status).toBe('unread');
  });
  test('Each Message must have a type whose value conforms to type: "info" | "warning" | "error" ', () => {
    const msg: MessageInterface | MessageOperationsInterface | Message = new Message(Message.generateArbitraryMessage(false))
    const msgTypes: MessageType[] = ['info', 'warning', 'error'];
    expect(msgTypes).toContain((msg as MessageInterface).type);
  });
  test('Each Message\'s Property could be read or set using getters and setters...', () => {
    const msg: Message = new Message(Message.generateArbitraryMessage(false))
    const msgTypes: MessageType[] = ['info', 'warning', 'error'];
    expect(msgTypes).toContain(msg .fetchMessageByID((msg as MessageInterface).id)?.type);
  });
  test('Each Message\'s Property could be read or set using getters and setters...', () => {
    const msg: Message = new Message(Message.generateArbitraryMessage(false))
    expect(typeof msg.getDuration()).toEqual('number');
  });
  test('Each Message\'s Body is specifically a String...', () => {
    const msg: Message = new Message(Message.generateArbitraryMessage(false))
    expect(typeof msg.getBody()).toEqual('string');
  });
  test('Each Message\'s Title is specifically a String...', () => {
    const msg: Message = new Message(Message.generateArbitraryMessage(false))
    expect(typeof msg.getTitle()).toEqual('string');
  });
  test('Each Message has date which is of Type Date..', () => {
    const msg: Message = new Message(Message.generateArbitraryMessage(false))
    expect(msg.getDate().constructor.name).toEqual('Date');
  });
});
