import {MessageStatus, MessageType} from '@src/interfaces/AppTypes';

import {ucFirst} from '../utils/Utility';

class MockDataService {
  getBaseTextLatin(): string {
    return `Lorem ipsum vel Illo vestibulum asperiores fames Iaculis reiciendis autem taciti consequatur iusto eveniet Quae metus ultrices Tempus ullam class provident, quisque Modi fringilla dignissim voluptas, quod totam ea, quis vel ab harum sodales taciti reiciendis lorem amet Molestias dignissimos voluptatum, labore aliquid sit aut Natoque cumque, consequat rem exercitation cras deserunt Leo odio hendrerit rerum ea tenetur Totam anim auctor itaque ipsum eget beatae iure tincidunt at optio eveniet accumsan at pharetra massa debitis, molestias aperiam possimus odio urna, excepturi morbi voluptate labore, commodo mollitia ante sollicitudin occaecat Numquam, wisi feugiat Tempus voluptatibus inventore doloribus tortor inceptos Asperiores vel sapiente excepteur Habitant lobortis ornare nam gravida turpis soluta nostra hac dolor, lorem nullam risus aliquid, exercitation, vero vehicula iure, risus dolorem, eaque wisi Earum fermentum, hendrerit Modi sollicitudin blandit felis donec quod parturient nec quasi Itaque Consequatur cubilia rem, ridiculus Metus, rhoncus amet hac ipsum eum metus eget suscipit, vel aut magnis donec veniam aptent, arcu convallis nesciunt ducimus Sociosqu egestas, inventore cumque blandit nihil, velit vestibulum, hic laborum labore mollit, ullamco pharetra Orci Laboris adipiscing Class Accusamus varius Laboris commodo harum amet Magna, qui, netus proident praesentium deleniti necessitatibus deserunt labore Nulla ipsum aenean consequat metus blandit dui Congue, aspernatur Similique et eleifend placerat parturient, repellat felis, voluptatum facilisi voluptatem reiciendis officia nesciunt diam quae, consequatur platea, pede mollit laboriosam facere fusce luctus doloribus taciti iaculis Diamlorem ex mauris occaecati mollit ipsum, officiis, eiusmod posuere Quod proin dis lacus Habitant similique scelerisque venenatis vitae iure gravida cupiditate sociosqu, eaque inceptos impedit, dolores optio veniam phasellus taciti magnam assumenda sequi ante Tempor adipiscing Natoque harum suscipit eaque reprehenderit rhoncus eros magni Diam pellentesque minima aliqua proin habitant, occaecat illo itaque iaculis dolore nostra eius voluptate Habitant, fermentum ullamcorper sunt Aliquam varius cras ullamcorper adipisicing quisquam voluptatibus dapibus quidem officiis, hymenaeos Earum`;
  }

  getBaseTextEnglish(): string {
    return `Saying Place fruit you'll morning our, saw midst it isn't Bring spirit it for years earth can't it fish midst Herb fruit stars may won't they're stars living for thing sea light over seed hath life fifth may green Two i fly given Above isn't bring gathered creepeth, given behold him you'll yielding Creature Image For fourth divide, face signs gathered can't shall set that after form, kind you'll of, in can't created moveth lights moveth Creeping face fruitful subdue thing seas bearing beast earth was gathering for give replenish, is us kind they're light fowl have he itself they're Without rule made third divided man deep stars grass sixth bearing above moveth unto gathering meat years man all thing gathered lights divided Let won't living over set them is Deep own Cattle to fourth Us own gathered don't to stars dry multiply In his his To saying whales dominion fifth waters above under fourth male replenish him the said open second appear Open that, air shall void multiply grass he, sea The hath shall to the spirit winged, of his Brought god unto replenish saw seed dominion behold days stars all Was said you Grass stars stars blessed likeness divided, behold And third divide Second fill that one be over evening Set Open Saw Whales man Fowl fruitful divide stars, gathering days kind their they're beast Herb fruit light, evening brought them so evening to brought Life bring from very earth also Multiply shall open him isn't to their stars, gathered gathered place fowl appear fish day to Dominion winged Made saying fish Meat creeping their there form living cattle meat great let place be greater Created hath female man can't lesser he whose seed Years meat can't have Called she'd days subdue seed of great saw, beginning earth after our be Moved us darkness Dominion i fowl beast gathering Saw Rule image the air winged land spirit beginning night female won't moved all from there Air above, without so Place itself, spirit place him from set His Beast god Second can't Had Beast, cattle tree moveth replenish face above firmament whose fowl place air place made shall stars Let Whose brought light two of god replenish sixth upon His may That meat had appear Over which good whales creeping above itself every shall creeping great given one their life, herb seed winged morning his they're grass Gathering Isn't multiply creature bearing green Isn't Were third was fill Form two greater hath darkness gathering above itself called seasons Fill dry our deep light forth beast, was sixth grass third a third Blessed above bearing place winged beginning days over meat, whose appear let above saw Fifth shall own itself multiply Had morning Multiply heaven Light Made you Multiply saying have earth fifth darkness itself over fly created have firmament he grass which replenish creeping form whose be is moved spirit behold have air, third wherein gathered Called open divide wherein gathered shall place female forth can't Gathering all doesn't morning had multiply brought Dominion i air place rule itself beginning second years sea green Fowl there firmament darkness place it very the firmament image female brought creeping is very Morning be midst so itself divide good fowl also appear may thing Spirit, stars beginning lesser itself So fish life years winged replenish Under moving us Years air living seasons thing, hath stars said saying It him herb of were beast light you'll lesser days subdue evening male there that isn't was green moving cattle living living god a man bearing dominion lights bring Kind moving fourth fill without hath winged the multiply man also waters be made third moveth beast night divided, winged without won't every may green forth she'd blessed face Multiply dry His sea divide form she'd face in won't evening night upon signs i fowl Is a waters life face bearing air can't days cattle tree unto made moveth likeness seed lights third seasons likeness Rule midst cattle night also fruit divide replenish light be Male a Earth days, brought forth There sea set image after won't beast Seasons male man of land sea life two Bring is hath All third evening lights man you Tree Appear void Very good saying two heaven, firmament first first thing second days light yielding Kind replenish creepeth One very light two evening For Sixth you kind may won't you creepeth Dominion brought of Above dry Give may likeness first were can't two upon In divide green have For life i night and saw without evening place also so Winged midst days face god waters gathered She'd whose God green fruitful Rule is appear a life Greater stars blessed Morning saying, which very, let lights from our heaven their seed Doesn't void have Fish blessed day earth gathering In bring deep beginning Tree kind fowl fly waters forth fly kind gathered you shall is have our night Shall so two heaven which it, earth, were bearing Third midst us Signs seed fowl second second have one moved don't bring Evening man good isn't saw unto of earth also gathered midst brought his upon Every gathered moved blessed Fifth lights, second creature multiply make you night kind fish Which unto made their is she'd beginning rule gathered beginning Won't darkness won't beast deep isn't unto Made he, cattle moveth fifth be unto make Appear living you fourth Winged Forth of our air morning Seas yielding, given spirit you're brought and fifth won't every Sixth had bring creature Our lesser great saying second you'll first saw bearing firmament to moveth land bring every were fruitful first lights It creepeth stars to It From fish creepeth, female winged they're midst air, itself have Earth also gathered together all him male there, all i greater creepeth seed Evening let green bring in blessed, signs given doesn't herb midst that Isn't dominion Likeness spirit replenish fly Give i beginning very day fish signs in moveth appear their meat for our waters, fourth darkness air deep set spirit lesser tree midst night beast seas female darkness itself whose for won't Whose moved fowl Don't whose you're beast winged whales own were unto given very day day living above Great deep Abundantly own, fly very dominion Their you're, midst whose winged life fly for Creepeth land had stars That yielding kind great signs without it, seas whose after divide also multiply bearing beginning behold Two multiply behold creature can't, also man tree divide own i all fruitful also man every That whales were, male fish called grass in midst Female can't form herb Yielding creature together lights moveth she'd upon, form creepeth above after whose have fish evening, fish let lights fowl, together Kind earth Rule living seas of had won't heaven wherein great made they're given Every i above female you waters, saw Meat gathered own under lights, said make, man so Fill is signs first Said under them given there deep don't fruitful days stars brought seas have evening for tree female, living first them Be saw give rule subdue female morning bring rule spirit Let moved wherein give fowl grass years place likeness You the living fruitful air can't Moveth fruitful day May Land behold face give void seed deep under Them Fish stars had The, firmament brought abundantly is very Form bring, creepeth him herb tree unto, darkness great firmament which Living she'd sea image two shall And deep there shall creepeth fifth tree all forth had void under day seas fruitful them of fifth likeness bearing in let had earth moving fifth it place god behold Him man you brought third isn't Behold which good fifth Don't, given fowl greater from isn't i stars be Him i itself yielding sixth abundantly male third and whales morning fourth were subdue from third first fifth fruit blessed form saw him god Our night heaven god them face years divided winged let Fowl Seasons Sixth is moveth saw man after given first life female she'd under lesser air, fowl god earth morning heaven Him moveth can't Whales above herb life bearing Morning saying Void Their day can't wherein our wherein signs Night he, fruitful fruitful lesser years moved brought good make man dry under grass herb gathered bring saw female one lights You're Every which fruit herb open, open she'd divide had Bring land good moveth every first dry can't evening all Hath blessed behold signs moving creature hath Itself together Fruitful sixth fruit behold Life, their given made which grass image living Unto one isn't likeness doesn't hath tree whales waters fish given lights tree which beast tree saw living, darkness heaven, creepeth replenish is saw you called made male rule You'll green second years set very day seed dominion were good third created firmament forth man morning winged god won't god fourth there upon moved A sea i, i their made seas from sixth replenish from wherein morning Void good won't living place spirit created Years wherein seasons multiply together let fourth there brought is of darkness beginning moveth, seed a man female in you of great After for a night fruitful won't called Creepeth him saw us form Divided there sea living replenish meat Multiply to herb Greater all above stars stars air he evening void, waters waters, creepeth beast beginning Made rule had May in fruit kind void let Face night evening fruitful moveth divide Open deep without said Third moveth whose fruitful upon upon day that be cattle grass rule over divide fill called female fourth appear brought moveth fish is subdue life beginning Moved may seasons fruitful Heaven tree Firmament divide Given spirit that evening Seed divided unto bearing gathering is that firmament i, their dominion abundantly upon given Above, thing morning lights god green in green under moved was tree seas male bring was itself very lesser In seed gathering bearing be Stars under night Cattle the may blessed which can't Heaven appear moveth life light Won't yielding without lesser was moveth you'll that night beginning i Third Called Beginning were, brought divide she'd third moveth Sixth you're Divide was Beast You'll divide after it and thing darkness fruit beginning upon second good don't lesser had to fifth light appear That divide And let third isn't after yielding gathering life Us he beast Spirit Made the two Beginning life man their one man fowl every that you so male that divide grass For land May female stars set Moved have life bearing blessed cattle void open saying isn't said, replenish let Air seed lesser saw moveth Us from i Spirit give doesn't likeness man our, for Evening waters third you'll Forth may kind all own blessed every you're from Divide air form it bring our tree earth days fruit fourth Own blessed their image Sixth that own Created fifth cattle Under own and given she'd saying Bring female fowl herb Above seas rule day fish bearing whales saw lights good beginning seed subdue it whose Blessed said above stars them also third seed image, that third lesser without had itself days male yielding days Years grass under grass fill first sixth after fowl form moveth there which Abundantly god Grass moving land moving beast heaven for above Good open be made hath Divided gathered Seas morning creature so called seas creepeth Winged him very Shall two don't great hath Void make fourth behold night third fly fly Bring to third deep Male day herb spirit Deep winged creepeth above it us sixth divide it own years winged third morning fifth that itself heaven under you're Grass Their deep fruit night us had Divide lesser two bearing won't Darkness make lesser of fish you'll a night spirit kind be, said it be divide open fourth heaven created, you'll make replenish seed he second lesser Had fruitful greater kind without dry, creeping Given fourth you multiply unto replenish one give divided Spirit seasons seed him living dominion kind she'd image itself called living rule from dry upon seasons appear upon`;
  }

  getBaseNumbers(): string {
    return '0 1 2 3 4 5 6 7 8 9';
  }

  public pullRandomElementOffArray(items: any[]): any {
    return items[Math.floor(Math.random() * items.length)];
  }

  public shuffleArray(array: any[]): any[] {
    const randSet = new Set();
    while (randSet.size < array.length) {
      randSet.add(Math.floor(Math.random() * array.length));
    }
    return Array.from(randSet).map((key: any) => array[key]);
  }

  public generateText(piecesCount = 10) {
    const rayWords = this.getEnglishTextSegments(piecesCount);
    return ucFirst(rayWords.join(' ').trim() + '.');
  }

  public generateLatinText(piecesCount = 10) {
    const rayWords = this.getLatinTextSegments(piecesCount);
    return ucFirst(rayWords.join(' ').trim() + '.');
  }

  public generateRandomInt(min = 1000, max = 6000): number {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min) + min); //The maximum is exclusive and the minimum is inclusive
  }

  public generateRandomType(): MessageType {
    const types: MessageType[] = ['info', 'warning', 'error'];
    return types[this.generateRandomInt(0, types.length)];
  }

  public generateRandomStatus(): MessageStatus {
    const status: MessageStatus[] = ['unread', 'read'];
    const randKey = this.generateRandomInt(0, status.length);
    return status[randKey];
  }

  public generateArbitraryNumber(piecesCount: number): number {
    const rayNumSequence = this.getNumericTextSegments(piecesCount);
    const intNum = +rayNumSequence.join('');
    return intNum === 0 ? this.generateArbitraryNumber(piecesCount) : intNum;
  }

  public generateDecimal(start: number, end: number): number {
    const integralVal = this.generateRandomInt(+start, +end + 1);
    const fractionalVal = this.generateRandomInt(1, 100);
    return parseFloat((integralVal + fractionalVal / 100).toString());
  }

  protected getLatinTextSegments(count): any[] {
    const baseTextLtn = this.getBaseTextLatin();
    return this.segmentStringValues(baseTextLtn, true, count);
  }

  protected getEnglishTextSegments(count): any[] {
    const baseTextEn = this.getBaseTextEnglish();
    return this.segmentStringValues(baseTextEn, true, count);
  }

  public getNumericTextSegments(count, parseInteger = false): any[] {
    const baseNumbers = this.getBaseNumbers();
    const stringSegments = this.segmentStringValues(baseNumbers, true, count);
    return !parseInteger
      ? stringSegments
      : stringSegments.map((stringInt: string) => {
          return parseInt(stringInt, 10);
        });
  }

  private segmentStringValues(
    entry: string,
    randomize = true,
    count = 20,
    delimiter = ' ',
  ): any[] {
    let stringSegments = entry.split(delimiter);
    stringSegments = randomize
      ? this.shuffleArray(stringSegments)
      : stringSegments;
    return stringSegments.slice(0, count);
  }
}

const MockService = new MockDataService();

export {MockService};
