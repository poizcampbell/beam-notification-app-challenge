import {
  MessageInterface,
  MessageOperationsInterface,
} from '../interfaces/MessageInterface';
import {storeMan} from '../utils/BrowserStorageManager';
import {generateQuadRandomHash} from '../utils/Utility';

import {NotificationRenderer} from '../components/NotificationRenderer';
import {MessageStatus, MessageType} from '../interfaces/AppTypes';
import {MockService as MS } from '../services/MockDataService';

class Message implements MessageOperationsInterface, MessageInterface {
  id: string;
  type: MessageType;
  status: MessageStatus;
  body: string;
  date: Date;
  title = '';
  duration?: number= 5000;

  constructor(config) {
    this.initialize(config);
  }

  initialize(config): void {
    let loopKey;
    for (loopKey in config) {
      if (config.hasOwnProperty(loopKey) || loopKey in this) {
        this[loopKey] = config[loopKey];
      }
    }
  }

  deleteMessage(msgID: string | number): void {}

  fetchMessageByID(msgID: string | number): MessageInterface | null {
    return this.id === msgID ? this : null;
  }

  fetchUnreadMessages(msgID: string | number): MessageInterface[] {
    return [];
  }

  markMessageAsRead(msgID: string | number): void {
    this.status = 'read';   // this.status === 'read' ? 'unread' : 'read';
  }

  markMessageAsUnRead(msgID: string | number): void {
    this.status = 'unread';

  }

  positionMessage(msgID: string | number): void {}

  renderMessage(msgID: string | number): void {
    const msg: MessageInterface = storeMan.getProp(
      `beam_msg_${msgID}`,
      'notification',
      {},
    );
    msg.date = new Date(msg.date);
    const renderer = new NotificationRenderer(msg);
    renderer.render();
  }

  public static autoDeleteMessages(allMessages, deleteReadMessages = '') {
    if (parseInt(deleteReadMessages, 10) === 1) {
      const result = allMessages
        .filter((message) => message.status !== 'read')
        .map((notification) => {
          return {
            [`beam_msg_${notification.id}`]: {notification: notification},
          };
        });

      const filteredStoreData = {beam: {}};
      for (const msg of result) {
        const msgVal: MessageInterface = (Object.values(
          msg,
        )[0] as unknown) as MessageInterface;
        filteredStoreData.beam[
          `beam_msg_${msgVal['notification'].id}`
        ] = msgVal;
      }
      storeMan.store = filteredStoreData;
      storeMan.save();
      allMessages = (Object.values(storeMan.getRoot({})) as any).map(
        (msg) => msg.notification,
      );
    }
    return allMessages;
  }

  tagMessage(msgID: string | number): void {}

  getId(): string {
    return this.id;
  }

  setId(value: string) {
    this.id = value;
  }

  getType(): MessageType {
    return this.type;
  }

  setType(value: MessageType) {
    this.type = value;
  }

  getStatus(): MessageStatus {
    return this.status;
  }

  setStatus(value: MessageStatus) {
    this.status = value;
  }

  getBody(): string {
    return this.body;
  }

  setBody(value: string) {
    this.body = value;
  }

  getDate(): Date {
    return this.date;
  }

  setDate(value: Date) {
    this.date = value;
  }

  getTitle(): string {
    return this.title;
  }

  setTitle(value: string) {
    this.title = value;
  }

  getDuration(): number | undefined {
    return this.duration;
  }

  setDuration(value: number | undefined) {
    this.duration = value;
  }

  public static generateArbitraryMessage(save = true): MessageInterface {
    const notification: MessageInterface = {
      id: generateQuadRandomHash(32),
      type: MS.generateRandomType(),
      status: 'unread',
      body: MS.generateText(MS.generateRandomInt(15, 30)),
      date: new Date(),
      duration: MS.generateRandomInt(10000, 1000000),
      title: MS.generateText(MS.generateRandomInt(7, 15)),
    };
    if(save){
      storeMan.setProp(
        `beam_msg_${notification.id}`,
        'notification',
        {},
      );
    }
    return notification;
  }
}

export {Message};
