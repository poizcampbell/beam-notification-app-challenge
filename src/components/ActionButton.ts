import {MessageStatus} from '../interfaces/AppTypes';
import {dispatchCustomEvent} from '../utils/Utility';

class ActionButton {
  private btnInstance: HTMLElement;

  constructor(
    private statusOrClassName: MessageStatus | string,
    private label: string,
    private btnTag = 'BUTTON',
    private extraBtnClass = '',
  ) {
    this.create();
  }

  create(): HTMLElement {
    this.btnInstance = document.createElement(this.btnTag);
    this.btnInstance.classList.add(
      'action-button',
      `action-button__${this.statusOrClassName}`,
    );
    if (this.extraBtnClass) {
      this.btnInstance.classList.add(`action-button__${this.extraBtnClass}`);
    }
    this.btnInstance.innerHTML = this.label;
    this.btnInstance.addEventListener('click', ActionButton.handleButtonClick);
    this.btnInstance.addEventListener(
      'mouseover',
      ActionButton.handleMouseOverOnButton,
    );
    this.btnInstance.addEventListener(
      'mouseout',
      ActionButton.handleMouseOutFromButton,
    );
    return this.btnInstance;
  }

  private static handleButtonClick(e: MouseEvent): void {
    dispatchCustomEvent('btnClick', e.currentTarget as HTMLElement, e);
  }

  private static handleMouseOutFromButton(e: MouseEvent): void {
    dispatchCustomEvent('btnMouseOut', e.currentTarget as HTMLElement, e);
  }

  private static handleMouseOverOnButton(e: MouseEvent): void {
    dispatchCustomEvent('btnMouseOver', e.currentTarget as HTMLElement, e);
  }
}

export {ActionButton};
