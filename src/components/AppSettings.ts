import tippy from 'tippy.js';

import {FormElementInterface} from '../interfaces/FormElementInterface';
import {storeMan, storeManConfig} from '../utils/BrowserStorageManager';
import {
  dispatchCustomEvent,
  filterAnimationClasses,
  getDefaultToolTipConfig,
} from '../utils/Utility';

import {ActionButton} from '../components/ActionButton';
import {app} from '../components/App';

class AppSettings {
  iconClass = 'fa fa-cog fa-3x';
  triggerButton: HTMLElement;
  settingsPanelsVisible = false;
  formWrapper: HTMLElement;
  updateSettingsButton: HTMLButtonElement;
  appConfig: {[keyValuePair: string]: string};
  static instance: AppSettings;
  static APP_PANEL_ID = 'beam-notification-app';

  private constructor() {
    this.appConfig = storeManConfig.getProp('settings', 'current', {});
    if (!this.triggerButton) {
      const settingsBtn: ActionButton = new ActionButton(
        'settings-trigger',
        ` <span class="${this.iconClass}"></span>`,
      );
      this.triggerButton = settingsBtn.create();
      this.createSettingForm();
      document.body.appendChild(this.formWrapper);
    }
  }

  public static getInstance(): AppSettings {
    if (!AppSettings.instance) {
      AppSettings.instance = new AppSettings();
    }
    return AppSettings.instance;
  }

  mount(): void {
    document.body.appendChild(this.triggerButton);
    tippy(
      this.triggerButton,
      getDefaultToolTipConfig(
        `Toggle <strong class='accent'>«Settings»</strong> Pane`,
      ),
    );
    this.bindActionEventsToButtons();
    const triggerBtnCoordinates: DOMRect = this.triggerButton.getBoundingClientRect();
    this.formWrapper.style.top = `${triggerBtnCoordinates.height + 5}px`;
  }

  private bindActionEventsToButtons() {
    this.triggerButton.addEventListener('btnClick', (e) => {
      this.formWrapper.style.display = 'block';
      this.settingsPanelsVisible = !this.settingsPanelsVisible;
      if (!document.body.contains(this.formWrapper)) {
        this.settingsPanelsVisible = true;
        document.body.appendChild(this.formWrapper);
      }
      this.handleAppSettingsPanelRendering();
    });
  }

  private handleAppSettingsPanelRendering(): void {
    this.formWrapper = filterAnimationClasses(this.formWrapper);
    if (this.settingsPanelsVisible) {
      this.formWrapper.classList.add(
        'notification-component-animate',
        'nt-comp-animate__fadeSlideInNotificationWrapper',
      );
    } else {
      this.formWrapper.classList.add(
        'notification-component-animate',
        'nt-comp-animate__fadeSlideOutNotificationWrapper',
      );
    }
    this.formWrapper.addEventListener('animationend', (e) => {
      if (e.animationName === 'fadeSlideOutNotificationWrapper') {
        this.formWrapper.style.display = 'none';
      }
    });
  }

  private createSettingForm(): HTMLElement {
    // FORM WRAPPER:
    const defaultFormFieldConfig: FormElementInterface = this.fetchDefaultFormConfig();
    this.formWrapper = document.createElement('DIV');
    this.formWrapper.id = 'app-setting__form-wrapper';
    this.formWrapper.classList.add('app-setting__form-wrapper');
    this.formWrapper.style.display = 'none';

    // FORM WRAPPER: INNER
    const formWrapperInner = document.createElement('DIV');
    formWrapperInner.id = 'app-setting__form-wrapper';
    formWrapperInner.classList.add(
      'app-setting__form-wrapper--inner',
      'app-setting__form-wrapper--content',
    );

    // SETTINGS TITLE:
    const settingsTitle: HTMLElement = AppSettings.createHTMLElement({
      el: 'H1',
      className: 'app-setting__heading',
      innerContent: 'Beam Notification Settings.',
    });

    /** FORM ELEMENTS: */
    // 1. FIELD: `showAllUnreadMessages`
    const showUnread: HTMLElement = this.createFormElement(
      defaultFormFieldConfig,
    );

    // 2. FIELD: `deleteReadMessages`
    const deleteReadMessagesConfig = {
      ...defaultFormFieldConfig,
      inputName: 'deleteReadMessages',
      inputID: 'deleteReadMessages',
      label: 'Auto-Delete all read Messages?',
      inputValue: this.appConfig.deleteReadMessages ?? 'no',
    };
    const deleteRead: HTMLElement = this.createFormElement(
      deleteReadMessagesConfig,
    );

    // 3. FIELD: `notificationOutputMedium`
    const outputMediumConfig = {
      ...defaultFormFieldConfig,
      options: {
        Browser: 'Browser',
        Console: 'Console',
        'Native Alert': 'Native Alert',
      },
      inputType: 'select',
      inputName: 'notificationOutputMedium',
      inputID: 'notificationOutputMedium',
      label: 'Notification Output Medium:',
      inputValue: this.appConfig.notificationOutputMedium ?? 'Browser',
    };
    const notificationOutputMedium: HTMLElement = this.createFormElement(
      outputMediumConfig,
    );

    // 4. FIELD: `maxNotificationsPerSession`
    const maxNotificationsPerSession = {
      ...defaultFormFieldConfig,
      inputValue: this.appConfig.maxNotificationsPerSession ?? '5',
      inputName: 'maxNotificationsPerSession',
      label: 'Maximum Notifications per Session:',
      inputType: 'number',
      min: '1',
      max: '20',
      step: '1',
      inputID: 'maxNotificationsPerSession',
    };
    const maxMessage: HTMLElement = this.createFormElement(
      maxNotificationsPerSession,
    );

    // 5. FIELD: `notificationsLifeSpan`
    const notificationsLifeSpan = {
      ...defaultFormFieldConfig,
      inputValue: this.appConfig.notificationsLifeSpan ?? '5',
      inputName: 'notificationsLifeSpan',
      label: 'Notifications Life-Span (MS):',
      inputType: 'number',
      min: '1000',
      max: '20000',
      step: '1000',
      inputID: 'notificationsLifeSpan',
    };
    const notificationsDuration: HTMLElement = this.createFormElement(
      notificationsLifeSpan,
    );

    // 6. FIELD: `showAllNotifications`
    const showAllNotificationsConfig = {
      ...defaultFormFieldConfig,
      inputValue: this.appConfig.notificationPreference,
      inputName: 'notificationPreference',
      label: 'Show All Notifications:',
      inputType: 'radio',
      inputID: 'showAllNotifications',
      options: {value: 'showAllNotifications'},
    };
    const showAllNotifications: HTMLElement = this.createFormElement(
      showAllNotificationsConfig,
    );

    // 7. FIELD: `showOnlyInfoNotifications`
    const showOnlyInfoNotificationsConfig = {
      ...defaultFormFieldConfig,
      inputValue: this.appConfig.notificationPreference,
      inputName: 'notificationPreference',
      label: 'Show only Info Notifications:',
      inputType: 'radio',
      inputID: 'showOnlyInfoNotifications',
      options: {value: 'showOnlyInfoNotifications'},
    };
    const showOnlyInfoNotifications: HTMLElement = this.createFormElement(
      showOnlyInfoNotificationsConfig,
    );

    // 8. FIELD: `showOnlyWarnNotifications`
    const showOnlyWarnNotificationsConfig = {
      ...defaultFormFieldConfig,
      inputValue: this.appConfig.notificationPreference,
      inputName: 'notificationPreference',
      label: 'Show only Warning Notifications:',
      inputType: 'radio',
      inputID: 'showOnlyWarnNotifications',
      options: {value: 'showOnlyWarnNotifications'},
    };
    const showOnlyWarnNotifications: HTMLElement = this.createFormElement(
      showOnlyWarnNotificationsConfig,
    );

    // 9. FIELD: `showOnlyErrorNotifications`
    const showOnlyErrorNotificationsConfig = {
      ...defaultFormFieldConfig,
      inputValue: this.appConfig.notificationPreference,
      inputName: 'notificationPreference',
      label: 'Show only Error Notifications:',
      inputType: 'radio',
      inputID: 'showOnlyErrorNotifications',
      options: {value: 'showOnlyErrorNotifications'},
    };
    const showOnlyErrorNotifications: HTMLElement = this.createFormElement(
      showOnlyErrorNotificationsConfig,
    );

    // 10. FIELD: `sortByDateAsc`
    const sortByDateAscConfig = {
      ...defaultFormFieldConfig,
      inputValue: this.appConfig.sortBy,
      inputName: 'sortBy',
      label: 'Sort by Date (ASC):',
      inputType: 'radio',
      inputID: 'sortByDateAsc',
      options: {value: 'sortByDateAsc'},
    };
    const sortByDateAsc: HTMLElement = this.createFormElement(
      sortByDateAscConfig,
    );

    // 11. FIELD: `sortByDateDesc`
    const sortByDateDescConfig = {
      ...defaultFormFieldConfig,
      inputValue: this.appConfig.sortBy,
      inputName: 'sortBy',
      label: 'Sort by Date (DESC):',
      inputType: 'radio',
      inputID: 'sortByDateDesc',
      options: {value: 'sortByDateDesc'},
    };
    const sortByDateDesc: HTMLElement = this.createFormElement(
      sortByDateDescConfig,
    );

    // 11. FIELD: `sortByDateDesc`
    const sortByTypeConfig = {
      ...defaultFormFieldConfig,
      inputValue: this.appConfig.sortBy,
      inputName: 'sortBy',
      label: 'Sort by Type:',
      inputType: 'radio',
      inputID: 'sortByType',
      options: {value: 'sortByType'},
    };
    const sortByType: HTMLElement = this.createFormElement(
      sortByTypeConfig,
    );

    // 12. FIELD: `sortByDateDesc`
    const sortByStatusConfig = {
      ...defaultFormFieldConfig,
      inputValue: this.appConfig.sortBy,
      inputName: 'sortBy',
      label: 'Sort by Status:',
      inputType: 'radio',
      inputID: 'sortByStatus',
      options: {value: 'sortByStatus'},
    };
    const sortByStatus: HTMLElement = this.createFormElement(
      sortByStatusConfig,
    );

    // 10. BUTTON: `saveButton`
    this.updateSettingsButton = <HTMLButtonElement>(
      new ActionButton(
        'app-setting__save-config',
        `<span class='fa fa-cloud-download '></span> Save`,
      ).create()
    );
    const formGroupBtn: HTMLElement = document.createElement('DIV');
    formGroupBtn.classList.add('app-setting__form-group');
    formGroupBtn.append(this.updateSettingsButton);

    formWrapperInner.append(
      notificationOutputMedium,
      maxMessage,
      notificationsDuration,
      showAllNotifications,
      showOnlyInfoNotifications,
      showOnlyWarnNotifications,
      showOnlyErrorNotifications,
      showUnread,
      deleteRead,
      sortByDateAsc,
      sortByDateDesc,
      sortByType,
      sortByStatus,
      formGroupBtn,
    );
    this.formWrapper.append(settingsTitle, formWrapperInner);

    this.handleFormSave();

    return this.formWrapper;
  }

  fetchDefaultFormConfig(): FormElementInterface {
    return {
      label: 'Show all unread Messages Only?',
      placeholder: 'Please select...',
      inputType: 'checkbox',
      inputValue: this.appConfig.showAllUnreadMessages ?? '',
      inputName: 'showAllUnreadMessages',
      labelClass: '',
      inputClass: 'app-setting__form-field--show-unread',
      inputID: 'showAllUnreadMessages',
      labelID: '',
      options: {value: ''},
      dataAttributes: {'data-test': 'test'},
    };
  }

  createFormElement(config: FormElementInterface): HTMLElement {
    // FORM ELEMENT GROUP:
    const formGroup: HTMLElement = document.createElement('DIV');
    formGroup.classList.add('app-setting__form-group');

    // FORM-FIELD ELEMENT + LABEL:
    let formFieldElement: HTMLFormElement;
    switch (config.inputType.toLowerCase()) {
      case 'select':
        formFieldElement = (this.craftDropDown(
          config,
        ) as unknown) as HTMLFormElement;
        break;

      case 'radio':
        formFieldElement = (this.craftSoloRadioButton(
          config,
        ) as unknown) as HTMLFormElement;
        break;

      case 'checkbox':
        formFieldElement = (this.craftSoloCheckBox(
          config,
        ) as unknown) as HTMLFormElement;
        break;

      case 'number':
      case 'text':
      case 'email':
      case 'tel':
      case 'search':
      default:
        formFieldElement = (this.craftTextInputField(
          config,
        ) as unknown) as HTMLFormElement;
        break;
    }
    const formFieldLabel: HTMLLabelElement = this.craftFieldLabel(config);

    // PACK FORM-ELEMENT COMPONENTS INTO FORM GROUP:
    formGroup.append(formFieldLabel, formFieldElement);

    return formGroup;
  }

  craftFieldLabel(config: FormElementInterface): HTMLLabelElement {
    const fieldLabel: HTMLLabelElement = <HTMLLabelElement>(
      document.createElement('LABEL')
    );
    fieldLabel.htmlFor = config.inputID as string;
    fieldLabel.classList.add(`app-setting__form-field-label`);
    fieldLabel.innerText = config?.label as string;
    if (config?.labelClass) {
      fieldLabel.classList.add(config.labelClass);
    }
    return fieldLabel;
  }

  craftTextInputField(config: FormElementInterface): HTMLInputElement {
    // FORM ELEMENT INPUT:
    const inputElement: HTMLInputElement = <HTMLInputElement>(
      document.createElement('INPUT')
    );
    inputElement.type = config?.inputType.toLowerCase() ?? 'text';
    inputElement.classList.add(`app-setting__form-field`);

    if (config?.inputClass) {
      inputElement.classList.add(config.inputClass);
    }
    if (config?.min) {
      inputElement.setAttribute('min', config.min);
    }
    if (config?.max) {
      inputElement.setAttribute('max', config.max);
    }
    if (config?.step) {
      inputElement.setAttribute('step', config.step);
    }
    if (config?.placeholder) {
      inputElement.setAttribute('placeholder', config.placeholder);
    }

    inputElement.id = `${config?.inputID}`;
    inputElement.value = (config?.inputValue as string) ?? '';
    inputElement.name = config.inputName;
    return inputElement;
  }

  craftDropDown(config: FormElementInterface): HTMLSelectElement {
    const selectElement: HTMLSelectElement = <HTMLSelectElement>(
      document.createElement('SELECT')
    );
    selectElement.name = config.inputName;
    if (config.options) {
      for (const optionValue in config.options) {
        if (config.options.hasOwnProperty(optionValue)) {
          const opt: HTMLOptionElement = <HTMLOptionElement>(
            document.createElement('OPTION')
          );
          opt.value = optionValue;
          opt.innerText = config.options[optionValue];
          if (
            config?.inputValue &&
            config.inputValue.toLowerCase() === optionValue.toLowerCase()
          ) {
            opt.setAttribute('selected', 'selected');
          }
          selectElement.add(opt as HTMLOptionElement);
        }
      }
    }
    return selectElement;
  }

  craftSoloCheckBox(config: FormElementInterface): HTMLInputElement {
    const checkBoxElement: HTMLInputElement = <HTMLInputElement>(
      document.createElement('INPUT')
    );
    checkBoxElement.type = 'checkbox';
    checkBoxElement.id = (config.inputID as string) ?? '';
    checkBoxElement.name = config.inputName;
    checkBoxElement.value = config.inputValue ?? '';
    if (config?.inputValue) {
      checkBoxElement.checked = true;
    }
    return checkBoxElement;
  }

  private craftSoloRadioButton(config: FormElementInterface): HTMLInputElement {
    const titleValue = config?.options ? config.options : {value: ''};
    const radioButtonElement: HTMLInputElement = <HTMLInputElement>(
      document.createElement('INPUT')
    );
    radioButtonElement.type = 'radio';
    radioButtonElement.id = (config.inputID as string) ?? '';
    radioButtonElement.name = config.inputName;
    radioButtonElement.value = titleValue.value;
    if (config?.inputValue === titleValue.value) {
      radioButtonElement.checked = true;
    }
    return radioButtonElement; // groupName: 'exclusiveRenderingByType'
  }

  private static createHTMLElement(config: {
    el: string;
    className: string;
    innerContent: string;
  }) {
    const htmlElement: HTMLElement = document.createElement(config.el);
    htmlElement.classList.add(config.className);
    htmlElement.innerHTML = config.innerContent;
    return htmlElement;
  }

  private handleFormSave(): void {
    this.updateSettingsButton.addEventListener('btnClick', (e: Event) => {
      const formElements: NodeListOf<any> = this.formWrapper.querySelectorAll(
        'input, select',
      );
      const formData: any = {};
      formElements.forEach((formElement: HTMLFormElement) => {
        if (formElement.getAttribute('type')?.toLowerCase() === 'checkbox') {
          formData[formElement.name] = '';
          if (((formElement as unknown) as HTMLInputElement).checked) {
            formData[formElement.name] = '1';
          }
        } else if (
          formElement.getAttribute('type')?.toLowerCase() === 'radio'
        ) {
          if (((formElement as unknown) as HTMLInputElement).checked) {
            formData[formElement.name] = formElement.value;
          }
        } else {
          formData[formElement.name] = formElement.value;
        }
      });
      storeManConfig.updateProp('settings', 'current', formData);
      dispatchCustomEvent('settingsUpdated', document.body, formData);
      // TODO: FIRST RENDER A NOTIFICATION TO INDICATE SETTINGS WAS UPDATED AND THEN REMOVE ELEMENT....

      this.formWrapper = filterAnimationClasses(this.formWrapper);
      this.formWrapper.classList.add(
        'notification-component-animate',
        'nt-comp-animate__bounceOffNotificationWrapper',
      );
      this.formWrapper.addEventListener('animationend', (e: AnimationEvent) => {
        if (e.animationName === 'bounceOffNotificationWrapper') {
          this.formWrapper.remove();
        }
      });
      const appPanel: HTMLElement = <HTMLElement>(
        document.querySelector(`#${AppSettings.APP_PANEL_ID}`)
      );
      if (appPanel) {
        appPanel.innerHTML = '';
      }
      app.reRender();
      // (new App()).mount();
    });
  }
}

export {AppSettings};
