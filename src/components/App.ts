import {
  MessageStatus,
  MessageType,
  NotificationPreferenceType,
} from '../interfaces/AppTypes';
import {MessageInterface} from '../interfaces/MessageInterface';
import {NEMI} from '../interfaces/NotificationEventManagementInterface';
import {Message} from '../models/Message';
import {MockService as MS} from '../services/MockDataService';
import {
  BrowserStorageManager,
  storeMan,
} from '../utils/BrowserStorageManager';
import {generateQuadRandomHash, playAudio} from '../utils/Utility';

import {AppSettings} from './AppSettings';
import {NotificationTriggerSlot} from './NotificationTriggerSlot';

class App {
  private config: any;
  private notificationPreference: NotificationPreferenceType;

  renderNotificationsBasedOnConfig(): void {
    const bsManager: BrowserStorageManager = new BrowserStorageManager(
      'session',
      'beamConfig',
    );
    this.config = bsManager.getProp('settings', 'current', {});
    this.notificationPreference = <NotificationPreferenceType>(
      this.config.notificationPreference
    );
    let allMessages: MessageInterface[] = (Object.values(
      storeMan.getRoot({}),
    ) as any).map((msg) => msg.notification);

    allMessages = Message.autoDeleteMessages(
      allMessages,
      this.config.deleteReadMessages,
    );

    switch (this.config.sortBy) {
      case 'sortByDateAsc':
        allMessages = allMessages.sort( (prev: MessageInterface, next: MessageInterface): number => {
          return (new Date(prev.date)).getTime() -(new Date(next.date)).getTime();
        });
        break;

      case 'sortByDateDesc':
        allMessages = allMessages.sort( (prev: MessageInterface, next: MessageInterface): number => {
          return (new Date(next.date)).getTime() - (new Date(prev.date)).getTime();
        });
        break;

      case 'sortByType':
        allMessages = allMessages.sort( (prev: MessageInterface, next: MessageInterface): number => {
          return (prev.type > next.type) ? -1 : 1;
        });
        break;

      case 'sortByStatus':
        allMessages = allMessages.sort( (prev: MessageInterface, next: MessageInterface): number => {
          return ((prev.status as MessageStatus) > (next.status as MessageStatus)) ? -1 : 1;
        });
        break;

    }

    const unReadMessages: MessageInterface[] = allMessages.filter(
      (message: MessageInterface) => {
        return message.status?.toLowerCase() === 'unread';
      },
    );
    const infoNotifications: MessageInterface[] = allMessages.filter(
      (message: MessageInterface) => {
        return message.type?.toLowerCase() === 'info';
      },
    );
    const warnNotifications: MessageInterface[] = allMessages.filter(
      (message: MessageInterface) => {
        return message.type?.toLowerCase() === 'warning';
      },
    );
    const errorNotifications: MessageInterface[] = allMessages.filter(
      (message: MessageInterface) => {
        return message.type?.toLowerCase() === 'error';
      },
    );

    if (this.config.notificationPreference === 'showAllNotifications') {
      if (this.filterAndRender(allMessages)) {
        return;
      }
    } else if (
      this.config.notificationPreference === 'showOnlyInfoNotifications'
    ) {
      if (this.filterAndRender(infoNotifications)) {
        return;
      }
    } else if (
      this.config.notificationPreference === 'showOnlyWarnNotifications'
    ) {
      if (this.filterAndRender(warnNotifications)) {
        return;
      }
    } else if (
      this.config.notificationPreference === 'showOnlyErrorNotifications'
    ) {
      if (this.filterAndRender(errorNotifications)) {
        return;
      }
    } else if (this.config.showAllUnreadMessages === '1') {
      if (this.filterAndRender(unReadMessages)) {
        return;
      }
    }
  }

  generateNotification(notificationType: MessageType): void {
    const dateObj: Date = new Date();
    const msgID: string = generateQuadRandomHash(32);
    const config: MessageInterface = {
      id: msgID,
      type: notificationType,
      status: 'unread',
      body: MS.generateText(MS.generateRandomInt(15, 30)),
      date: dateObj,
      duration: 5000,
      title: MS.generateText(MS.generateRandomInt(7, 15)),
    };
    const msg = new Message(config);
    const storeKey = `beam_msg_${msgID}`;
    storeMan.add(storeKey, 'notification', msg);
    msg.renderMessage(msgID);
  }

  manageNotificationGenerationEvents(config: NEMI[]): void {
    for (const setting of config) {
      setting.slot.addEventListener('slotClick', async (e: CustomEvent) => {
        this.generateNotification(setting.type);
        playAudio('assets/audio/iphone-sms-tone.mp3').then((audio) => {
          audio.remove();
        });
      });
    }
  }

  filterAndRender(notifications: MessageInterface[]): boolean {
    if (this.config.showAllUnreadMessages === '1') {
      for (const tmpMessage of notifications) {
        if (tmpMessage.status === 'unread') {
          const msg = new Message(tmpMessage);
          msg.renderMessage(tmpMessage.id);
        }
      }
      return true;
    } else {
      for (const tmpMessage of notifications) {
        const msg = new Message(tmpMessage);
        msg.renderMessage(tmpMessage.id);
      }
    }
    return false;
  }

  renderNotificationTriggerSlots(): void {
    const infoSlotTrigger: NotificationTriggerSlot = new NotificationTriggerSlot(
      `Trigger <strong class='accent'>«info»</strong> Notification`,
      'info',
    );
    const infoSlot: HTMLElement = infoSlotTrigger.create();

    const warnSlotTrigger: NotificationTriggerSlot = new NotificationTriggerSlot(
      `Trigger <strong class='accent'>«warning»</strong> Notification`,
      'warning',
    );
    const warnSlot: HTMLElement = warnSlotTrigger.create();

    const errorSlotTrigger: NotificationTriggerSlot = new NotificationTriggerSlot(
      `Trigger <strong class='accent'>«error»</strong> Notification`,
      'error',
    );
    const errorSlot: HTMLElement = errorSlotTrigger.create();

    this.manageNotificationGenerationEvents([
      {slot: infoSlot, type: 'info'},
      {slot: warnSlot, type: 'warning'},
      {slot: errorSlot, type: 'error'},
    ]);

    document.body.append(infoSlot, warnSlot, errorSlot);

    infoSlotTrigger.enableToolTip();
    warnSlotTrigger.enableToolTip();
    errorSlotTrigger.enableToolTip();
  }

  handleSettingsUpdate(): void {}

  mount(): boolean {
    this.renderNotificationTriggerSlots();
    AppSettings.getInstance().mount();
    this.renderNotificationsBasedOnConfig();
    this.handleSettingsUpdate();
    return true;
  }

  reRender(): boolean {
    this.renderNotificationsBasedOnConfig();
    return true;
  }
}

const app = new App();
export default App;
export {app};
