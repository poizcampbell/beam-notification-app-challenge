import {
  MessageStatus,
  NotificationOutputMedium,
  NotificationPreferenceType,
} from '../interfaces/AppTypes';
import {MessageInterface} from '../interfaces/MessageInterface';
import {Message} from '../models/Message';
import {storeMan, storeManConfig} from '../utils/BrowserStorageManager';
import {
  DateFilter,
  filterAnimationClasses,
  playAudio,
} from '../utils/Utility';

import {ActionButton} from '../components/ActionButton';
import {app} from '../components/App';

class NotificationRenderer {
  private readonly mountPoint: HTMLElement | null;
  private readonly dismissButton: HTMLElement;
  private readonly markAsUnReadButton: HTMLElement;
  private container: HTMLElement;
  private appConfig: any;
  private outputMedium: NotificationOutputMedium;
  private notificationPreference: NotificationPreferenceType;

  constructor(private message: MessageInterface) {
    this.appConfig = storeManConfig.getProp('settings', 'current', {});
    this.notificationPreference = <NotificationPreferenceType>(
      this.appConfig.notificationPreference
    );
    this.outputMedium = <NotificationOutputMedium>(
      this.appConfig.notificationOutputMedium
    );
    this.mountPoint = document.querySelector('#beam-notification-app');
    this.container = document.createElement('DIV');
    this.dismissButton = this.createDismissButton();
    this.markAsUnReadButton = this.createMarkAsUnReadButton();
    this.createMainContainer();
  }

  private createMainContainer(): HTMLElement {
    this.container.classList.add(
      'notification',
      'notification-component-animate',
      'nt-comp-animate__fadeSlideInNotificationWrapper',
      this.getNotificationClassByStatus(),
      this.getNotificationClassByType(),
    );
    this.container.setAttribute('id', this.message.id.toString());
    this.container.setAttribute('data-id', this.message.id.toString());
    this.container.setAttribute('data-status', this.message.status as string);
    return this.container;
  }

  render(): HTMLElement {
    this.appConfig = storeManConfig.getProp('settings', 'current', {});
    if (this.appConfig.showAllUnreadMessages) {
      if (this.message.status?.toLowerCase() === 'unread') {
        this.mountNotificationBox();
        return this.container;
      }
    }

    switch (this.notificationPreference) {
      case 'showOnlyInfoNotifications':
        if (this.message.type === 'info') {
          this.mountNotificationBox();
        }
        break;

      case 'showOnlyWarnNotifications':
        if (this.message.type === 'warning') {
          this.mountNotificationBox();
        }
        break;

      case 'showOnlyErrorNotifications':
        if (this.message.type === 'error') {
          this.mountNotificationBox();
        }
        break;

      case 'showAllNotifications':
      default:
        this.mountNotificationBox();
        break;
    }
    return this.container;
  }

  private mountNotificationBox(): void {
    switch (this.outputMedium) {
      case 'Console':
        console.log(this.message);
        break;

      case 'Native Alert':
        alert(`${this.message.type.toUpperCase()}:\n${this.message.body}`);
        break;

      case 'Browser':
      default:
        this.container.appendChild(this.markAsUnReadButton);
        this.container.appendChild(this.createNotificationTimeBlock());
        this.container.appendChild(this.createNotificationBody());
        this.container.appendChild(this.dismissButton);
        if (this.mountPoint) {
          this.mountPoint.append(this.container);
        }
        this.bindActionEventsToButtons();
        break;
    }
  }

  private createNotificationBody(): HTMLElement {
    const msgBody = document.createElement('DIV');
    msgBody.classList.add('notification-renderer__body');
    msgBody.innerHTML = `
    <strong class='notification-type notification-type__${this.message.type}'>
      ${this.message.type.toUpperCase()}:
    </strong>&nbsp;${this.message.body}
    `;
    return msgBody;
  }

  private createNotificationTimeBlock(): HTMLElement {
    const timeBlock = document.createElement('DIV');
    const timeBlockInner = document.createElement('SPAN');
    timeBlock.classList.add('notification-renderer__date');
    timeBlockInner.classList.add('notification-renderer__date--content');
    timeBlockInner.innerHTML = DateFilter.swissDate(this.message.date, 'en');
    timeBlock.appendChild(timeBlockInner);
    return timeBlock;
  }

  private getNotificationClassByStatus(): string {
    return `notification__${this.message.status}`;
  }

  private getNotificationClassByType(): string {
    return `notification__${this.message.type}`;
  }

  private createDismissButton(): HTMLElement {
    return new ActionButton(
      this.message.status as MessageStatus,
      '<span class="fa fa-times-rectangle"></span>',
      'SPAN',
      'dismiss-msg',
    ).create();
  }

  private createMarkAsUnReadButton(): HTMLElement {
    const config = {
      btnClass:
        this.message.status?.toLowerCase() === 'read'
          ? 'notification-renderer__mark-as-unread'
          : 'notification-renderer__mark-as-read',
      btnLabel:
        this.message.status?.toLowerCase() === 'read'
          ? 'Mark as Unread'
          : 'Mark as Read',
    };
    return new ActionButton(
      this.message.status as MessageStatus,
      `<span class="fa fa-check-square"></span> ${config.btnLabel}`,
      'BUTTON',
      `${config.btnClass}`,
    ).create();
  }

  private bindActionEventsToButtons() {
    this.appConfig = storeManConfig.getProp('settings', 'current', {});
    const storeData = storeMan.store;
    let allMessages: MessageInterface[] = (Object.values(
      storeMan.getRoot({}),
    ) as any).map((msg) => msg.notification);
    // DISMISS BUTTON
    this.dismissButton.addEventListener('btnClick', (e) => {
      this.container = filterAnimationClasses(this.container);
      this.container.classList.add(
        'notification-component-animate',
        'nt-comp-animate__fadeSlideOutNotificationWrapper',
      );
      allMessages = Message.autoDeleteMessages(
        allMessages,
        this.appConfig.deleteReadMessages,
      );
      // PLAY AUDIO FILE:
      playAudio('assets/audio/mac-empty-trash-sound-effect.mp3').then(
        (audio) => {
          audio.remove();
        },
      );

      this.container.addEventListener('animationend', () => {
        this.mountPoint?.removeChild(this.container);
      });
    });

    // MARK-AS-READ BUTTON
    this.markAsUnReadButton.addEventListener('btnClick', (e) => {
      this.container = filterAnimationClasses(this.container);
      this.message.status =
        this.message.status?.toLowerCase() === 'read' ? 'unread' : 'read';
      const storeKey = `beam_msg_${this.message.id}`;
      storeMan.updateProp(storeKey, 'notification', this.message);

      const allMessages: MessageInterface[] = (Object.values(
        storeMan.getRoot({}),
      ) as any).map((msg) => msg.notification);
      Message.autoDeleteMessages(
        allMessages,
        this.appConfig.deleteReadMessages,
      );
      (document.querySelector(
        '#beam-notification-app',
      ) as HTMLElement).innerHTML = '';
      app.reRender();

      // PLAY AUDIO FILE:
      playAudio('assets/audio/Basso.m4a').then((audio) => {
        audio.remove();
      });
      this.container.classList.add(
        'notification-component-animate',
        'nt-comp-animate__bounceOffNotificationWrapper',
      ); //InDownBig');

      this.container.addEventListener('animationend', () => {
        this.mountPoint?.removeChild(this.container);
      });
    });
  }
}

export {NotificationRenderer};
