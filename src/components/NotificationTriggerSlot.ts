import tippy from 'tippy.js';

import {dispatchCustomEvent, getDefaultToolTipConfig} from '../utils/Utility';

class NotificationTriggerSlot {
  private slotInstance: HTMLElement;

  constructor(private title: string, private className?: string) {
    this.create();
  }

  create(): HTMLElement {
    this.slotInstance = document.createElement('DIV');
    this.slotInstance.id = `notification-trigger-slot__${this.className}`;
    this.slotInstance.classList.add('notification-trigger-slot');

    if (this.className) {
      this.slotInstance.classList.add(
        `notification-trigger-slot__${this.className}`,
      );
    }
    this.slotInstance.addEventListener(
      'click',
      NotificationTriggerSlot.handleButtonClick,
    );
    this.slotInstance.addEventListener('mouseover', (e) => {
      if (this.slotInstance.classList.contains('morph-out')) {
        this.slotInstance.classList.remove('morph-out');
      }
      this.slotInstance.classList.add('morph-in');
      NotificationTriggerSlot.handleMouseOverOnButton(e);
    });
    this.slotInstance.addEventListener('mouseout', (e) => {
      if (this.slotInstance.classList.contains('morph-in')) {
        this.slotInstance.classList.remove('morph-in');
      }
      this.slotInstance.classList.add('morph-out');
      NotificationTriggerSlot.handleMouseOutFromButton(e);
    });
    return this.slotInstance;
  }

  public enableToolTip(): void {
    if (this.title) {
      tippy(
        `#notification-trigger-slot__${this.className}`,
        getDefaultToolTipConfig(this.title),
      );
    }
  }

  private static handleButtonClick(e: MouseEvent): void {
    dispatchCustomEvent('slotClick', e.currentTarget as HTMLElement, e);
  }

  private static handleMouseOutFromButton(e: MouseEvent): void {
    dispatchCustomEvent('slotMouseOut', e.currentTarget as HTMLElement, e);
  }

  private static handleMouseOverOnButton(e: MouseEvent): void {
    dispatchCustomEvent('slotMouseOver', e.currentTarget as HTMLElement, e);
  }
}

export {NotificationTriggerSlot};
