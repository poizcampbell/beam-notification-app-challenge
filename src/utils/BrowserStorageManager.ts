class BrowserStorageManager {
  private smInstance: BrowserStorageManager;
  private smStore: any = {};
  private smData: any = {};

  constructor(private smAdapter = 'session', private smNamespace = 'default') {
    this.smInstance = this;
    this.init();
  }

  static exists(object: any, nullChecks = true): boolean {
    return nullChecks
      ? object !== undefined && object !== null
      : object !== undefined;
  }

  static extend(...restArgs: Array<any>): any {
    if (restArgs.length < 1) {
      return null;
    }
    for (let i = 1; i < restArgs.length; i++) {
      for (const key in restArgs[i]) {
        if (restArgs[i].hasOwnProperty(key)) {
          restArgs[0][key] = restArgs[i][key];
        }
      }
    }
    return restArgs[0];
  }

  init(): BrowserStorageManager {
    this.bake();
    // console.log('STORAGE INITIALIZED....');
    return this;
  }

  bake(): void {
    if (typeof Storage !== 'undefined') {
      this.syncStore();
      this.smStore[this.smNamespace] = this.isSet(
        this.smStore[this.smNamespace],
      )
        ? this.smStore[this.smNamespace]
        : {};
      if (this.smAdapter === 'session') {
        sessionStorage.setItem(this.smNamespace, JSON.stringify(this.smStore));
      } else if (this.smAdapter === 'local') {
        localStorage.setItem(this.smNamespace, JSON.stringify(this.smStore));
      }
    } else {
      console.log(`Sorry! No Web Storage support...`);
    }
  }

  getProp(tKey: string, prop: string, defVal: any): any {
    // FIRST CHECK THE DATA STORED IN THE CONTEXT OF THE THIS CLASS VARIABLE
    // IF NOTHING SHOW UP, TRY ACCESSING IT FROM THE STORE...
    // IF ALL FAILS RETURN NULL!!!
    this.syncStore();
    defVal = this.isSet(defVal) ? defVal : null;
    if (this.isSet(this.smStore[this.smNamespace][tKey], false)) {
      if (this.smStore[this.smNamespace][tKey][prop] !== undefined) {
        return this.smStore[this.smNamespace][tKey][prop];
      }
    }
    return defVal;
  }

  getRoot(defVal: any): any {
    // FIRST CHECK THE DATA STORED IN THE CONTEXT OF THE THIS CLASS VARIABLE
    // IF NOTHING SHOW UP, TRY ACCESSING IT FROM THE STORE...
    // IF ALL FAILS RETURN NULL!!!
    this.syncStore();
    defVal = this.isSet(defVal) ? defVal : null;
    if (this.isSet(this.smStore[this.smNamespace], false)) {
      return this.smStore[this.smNamespace];
    }
    return defVal;
  }

  getAll(tKey: string, defVal: any): any {
    this.syncStore();
    defVal = defVal === undefined ? {} : defVal;
    if (this.smStore[this.smNamespace][tKey] === undefined) {
      return defVal;
    }
    return this.smStore[this.smNamespace][tKey];
  }

  setProp(tKey: string, prop: string, propValue: any): BrowserStorageManager {
    this.syncStore();
    if (this.smStore[this.smNamespace][tKey] === undefined) {
      this.smStore[this.smNamespace][tKey] = this.smData;
    }
    this.smStore[this.smNamespace][tKey][prop] = propValue;
    // SAVE IMMEDIATELY TO STORE:
    this.save();
    return this;
  }

  updateProp(
    tKey: string,
    prop: string,
    propValue: any,
  ): BrowserStorageManager {
    return this.setProp(tKey, prop, propValue);
  }

  add(tKey: string, prop: string, propValue: any): BrowserStorageManager {
    this.setProp(tKey, prop, propValue);
    return this;
  }

  setAdapter(adapter: string): void {
    this.smAdapter = adapter;
  }

  setNamespace(namespace: string): void {
    this.smNamespace = namespace;
  }

  has(tKey: string, prop: string): boolean {
    this.syncStore();
    if (this.isSet(this.smStore[this.smNamespace][tKey], false)) {
      if (this.isSet(this.smStore[this.smNamespace][tKey][prop], false)) {
        return true;
      }
    }
    return false;
  }

  hasKey(tKey: string): boolean {
    return this.isSet(this.smStore[this.smNamespace][tKey], false);
  }

  storeHasKey(key: string): any | boolean {
    if (typeof Storage !== 'undefined') {
      let storedData: any = null;
      if (this.smAdapter === 'session') {
        storedData = window.sessionStorage.getItem(this.smNamespace);
      } else if (this.smAdapter === 'local') {
        storedData = window.localStorage.getItem(this.smNamespace);
      }
      if (this.isSet(storedData) && storedData) {
        const nSpacedData = JSON.parse(storedData);
        return nSpacedData[key];
      }
      return false;
    } else {
      alert(
        'This App will not run very well without Web Storage Support on your Browsers.\nPlease, install a Modern Browser with Web-Storage Support...',
      );
    }
    return false;
  }

  isSet(object: any, nullChecks = true): boolean {
    return BrowserStorageManager.exists(object, nullChecks);
  }

  syncStore(): BrowserStorageManager {
    // GET DATA FROM STORAGE IF IT EXISTS....
    // HOWEVER, IF WE HAVE A CLASS-SCOPE EQUIVALENT,
    // WE USE IT TO OVERRIDE THAT FROM STORAGE...
    let storedData: any = null;
    let confData = {};
    if (this.smAdapter === 'session') {
      storedData = window.sessionStorage.getItem(this.smNamespace);
    } else if (this.smAdapter === 'local') {
      storedData = window.localStorage.getItem(this.smNamespace);
    }
    if (storedData !== undefined && storedData) {
      confData = JSON.parse(storedData);
    }
    this.smStore = BrowserStorageManager.extend({}, this.smStore, confData);
    return this;
  }

  save(): BrowserStorageManager {
    if (typeof Storage !== 'undefined') {
      if (this.smAdapter === 'session') {
        sessionStorage.setItem(this.smNamespace, JSON.stringify(this.smStore));
      } else if (this.smAdapter === 'local') {
        localStorage.setItem(this.smNamespace, JSON.stringify(this.smStore));
      }
    } else {
      // Sorry! No Web Storage support..
    }
    return this;
  }

  get store(): any {
    return this.smStore;
  }

  set store(value: any) {
    this.smStore = value;
  }

  get data(): any {
    return this.smData;
  }

  set data(value: any) {
    this.smData = value;
  }

  set adapter(value: string) {
    this.smAdapter = value;
  }

  get adapter(): string {
    return this.smAdapter;
  }

  get namespace(): string {
    return this.smNamespace;
  }

  set namespace(value: string) {
    this.smNamespace = value;
  }

  get instance(): BrowserStorageManager {
    return this.smInstance;
  }

  set instance(value: BrowserStorageManager) {
    this.smInstance = value;
  }
}

const storeMan = new BrowserStorageManager('session', 'beam');
const storeManConfig = new BrowserStorageManager('session', 'beamConfig');

if (!storeManConfig.getProp('settings', 'current', null)) {
  storeManConfig.add('settings', 'current', {
    showAllUnreadMessages: '',
    deleteReadMessages: '1',
    notificationsLifeSpan: '5000',
    maxNotificationsPerSession: '10',
    notificationPreference: 'showAllNotifications',
    notificationOutputMedium: 'Browser',
    sortBy: 'sortByDateAsc',
  });
}

export {storeMan, BrowserStorageManager, storeManConfig};
