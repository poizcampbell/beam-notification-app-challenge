// GIVEN A LENGTH OF CHARACTERS: `length` & A GROUP SUB-LENGTH: `subLen`
// GENERATES A RANDOM HEX STRING GROUPED INTO BEADS OF `subLen`
// AND SEPARATED BY DASH "-"
const generateQuadRandomHash = (length = 16, subLen = 4) => {
  if (length % subLen !== 0) {
    length = length - (length % 4) + subLen;
  }
  const characters = '0123456789ABCDEF';
  let randomString = '';

  for (let i = 0; i < length; i++) {
    randomString += characters[Math.floor(Math.random() * characters.length)];
    randomString += (i + 1) % subLen === 0 ? '-' : '';
  }

  return randomString.replace(/-$/, '');
};

const ucFirst = (strInput: string): string => {
  return strInput[0].toUpperCase() + strInput.substr(1);
};

const ucWords = (strInput: string): string => {
  return strInput
    .split(/[\t\s ]+/g)
    .filter((char) => !!char)
    .map((char) => ucFirst(char))
    .join(' ');
};

const camelFy = (strInput: string): string => {
  // SPLIT THE STRING AT SOME BOUNDARY: [_\- \+]
  return strInput
    .split(/[_\- +]/giu)
    .filter((char) => !!char)
    .map((char, index) => (index === 0 ? char.toLowerCase() : ucFirst(char)))
    .join('');
};

const snakeFy = (strInput: string): string => {
  const camelCased = camelFy(strInput);
  let outStr = '';
  for (let i = 0; i < camelCased.length; i++) {
    const char = camelCased[i];
    if (isUpperCase(char)) {
      outStr += `_${char.toLowerCase()}`;
    } else {
      outStr += char;
    }
  }
  return outStr;
};

const isLowerCase = (char: string): boolean => {
  return char.trim().toLowerCase() === char.trim();
};

const isUpperCase = (char: string): boolean => {
  return char.trim().toUpperCase() === char.trim();
};

const dispatchCustomEvent = (
  eventName: string,
  domElement: HTMLElement,
  payload: any = null,
) => {
  const event = new CustomEvent(eventName, {detail: payload});
  domElement.dispatchEvent(event);
};

// DATE MANIPULATION
interface FilteredDateInterface {
  y: number;
  m: string | number;
  d: string | number;
  h: string | number;
  i: string | number;
  s: string | number;
  wd: string;
  ms: string;
}
const iconFall = '';
const iconSpring = '';
const iconSummer = '';
const iconWinter = '';

const DateFilter = {
  dayMap: {
    en: {
      0: {full: 'Sunday', ab: 'Sun.'},
      1: {full: 'Monday', ab: 'Mon.'},
      2: {full: 'Tuesday', ab: 'Tue.'},
      3: {full: 'Wednesday', ab: 'Wed.'},
      4: {full: 'Thursday', ab: 'Thur.'},
      5: {full: 'Friday', ab: 'Fri.'},
      6: {full: 'Saturday', ab: 'Sat.'},
    },
    de: {
      0: {full: 'Sonntag', ab: 'So.'},
      1: {full: 'Montag', ab: 'Mo.'},
      2: {full: 'Dienstag', ab: 'Di.'},
      3: {full: 'Mittwoch', ab: 'Mi.'},
      4: {full: 'Donnerstag', ab: 'Do.'},
      5: {full: 'Freitag', ab: 'Fr.'},
      6: {full: 'Samstag', ab: 'Sa.'},
    },
    fr: {
      0: {full: 'Dimanche', ab: 'Di.'},
      1: {full: 'Lundi', ab: 'Lu.'},
      2: {full: 'Mardi', ab: 'Ma.'},
      3: {full: 'Mercredi', ab: 'Me.'},
      4: {full: 'Jeudi', ab: 'Je.'},
      5: {full: 'Vendredi', ab: 'Ve.'},
      6: {full: 'Samedi', ab: 'Sa.'},
    },
  },

  monthMap: {
    en: {
      0: {full: 'January', ab: 'Jan.'},
      1: {full: 'February', ab: 'Feb.'},
      2: {full: 'March', ab: 'Mar.'},
      3: {full: 'April', ab: 'Apr.'},
      4: {full: 'May', ab: 'May.'},
      5: {full: 'June', ab: 'Jun.'},
      6: {full: 'July', ab: 'Jul.'},
      7: {full: 'August', ab: 'Aug.'},
      8: {full: 'September', ab: 'Sept.'},
      9: {full: 'October', ab: 'Oct.'},
      10: {full: 'November', ab: 'Nov.'},
      11: {full: 'December', ab: 'Dec.'},
    },
    de: {
      0: {full: 'Januar', ab: 'Jan.'},
      1: {full: 'Februar', ab: 'Feb.'},
      2: {full: 'März', ab: 'Mar.'},
      3: {full: 'April', ab: 'Apr.'},
      4: {full: 'Mai', ab: 'Mai.'},
      5: {full: 'Juni', ab: 'Jun.'},
      6: {full: 'Juli', ab: 'Jul.'},
      7: {full: 'August', ab: 'Aug.'},
      8: {full: 'September', ab: 'Sept.'},
      9: {full: 'Oktober', ab: 'Oct.'},
      10: {full: 'November', ab: 'Nov.'},
      11: {full: 'Dezember', ab: 'Dez.'},
    },
    fr: {
      0: {full: 'Janvier', ab: 'Jan.'},
      1: {full: 'Fevrier', ab: 'Fev.'},
      2: {full: 'Marz', ab: 'Mar.'},
      3: {full: 'Avril', ab: 'Avr.'},
      4: {full: 'Mai', ab: 'Mai.'},
      5: {full: 'Juin', ab: 'Jui.'},
      6: {full: 'Juillet', ab: 'Juil.'},
      7: {full: 'Aout', ab: 'Aout.'},
      8: {full: 'Septembre', ab: 'Sept.'},
      9: {full: 'Octobre', ab: 'Oct.'},
      10: {full: 'Novembre', ab: 'Nov.'},
      11: {full: 'Decembre', ab: 'Dec.'},
    },
  },

  prepositionsMap: {
    en: {
      at: 'at',
      on: 'on',
    },
    de: {
      at: 'um',
      on: 'am',
    },
    fr: {
      at: '&agrave;',
      on: 'le',
    },
  },

  seasonsPeriodicMap: {
    south: {
      // SOUTHERN HEMISPHERE
      fall: {start: {day: 1, month: 2}, end: {day: 31, month: 4}}, // AUTUMN
      spring: {start: {day: 1, month: 8}, end: {day: 31, month: 10}},
      summer: {start: {day: 1, month: 11}, end: {day: 29, month: 1}},
      winter: {start: {day: 1, month: 5}, end: {day: 31, month: 7}},
    },
    north: {
      // NORTHERN HEMISPHERE
      fall: {start: {day: 1, month: 8}, end: {day: 31, month: 10}}, // AUTUMN
      spring: {start: {day: 1, month: 2}, end: {day: 31, month: 4}},
      summer: {start: {day: 1, month: 5}, end: {day: 31, month: 7}},
      winter: {start: {day: 1, month: 11}, end: {day: 29, month: 1}},
    },
  },

  seasonsMap: {
    en: {
      fall: {label: 'Fall', icon: iconFall},
      spring: {label: 'Spring', icon: iconSpring},
      summer: {label: 'Summer', icon: iconSummer},
      winter: {label: 'Winter', icon: iconWinter},
    },
    de: {
      fall: {label: 'Herbst', icon: iconFall},
      spring: {label: 'Frühling', icon: iconSpring},
      summer: {label: 'Sommer', icon: iconSummer},
      winter: {label: 'Winter', icon: iconWinter},
    },
    fr: {
      fall: {label: 'Automne', icon: iconFall},
      spring: {label: 'Printemps', icon: iconSpring},
      summer: {label: 'Été', icon: iconSummer},
      winter: {label: 'Hiver', icon: iconWinter},
    },
  },

  seconds: (secondsInt: number) => {
    return DateFilter.zeroPrefix(secondsInt);
  },

  minutes: (minutesInt: number) => {
    return DateFilter.zeroPrefix(minutesInt);
  },

  hours: (hoursInt: number) => {
    return DateFilter.zeroPrefix(hoursInt);
  },

  date: (dayInt: number) => {
    return DateFilter.zeroPrefix(dayInt);
  },

  month: (monthInt: number) => {
    return DateFilter.zeroPrefix(monthInt);
  },

  swissDate: (dateObject: Date, lang = 'de'): string => {
    const dt = DateFilter.getFilteredDateObject(dateObject, lang);
    return `${dt.wd} ${dt.d} ${dt.ms} ${dt.y} ${
      (DateFilter.prepositionsMap as any)[lang].at
    } ${dt.h}:${dt.i}:${dt.s}`; //  -- ${dt.m}
  },

  // PLEASE, REVISIT...
  format: (dateObject: Date, lang = 'de', formatSpecifier = []) => {
    const dt: FilteredDateInterface = DateFilter.getFilteredDateObject(
      dateObject,
      lang,
    );
    return `${dt.wd}; ${dt.d}.${dt.m}.${dt.y} ${dt.h}:${dt.i}:${dt.s}`;
  },

  getSeason: (dateObject: Date, lat = 0, lang = 'de') => {
    const seasonData = {
      label: null,
      icon: null,
    };
    const hemisphere = lat < 0 ? 'south' : 'north';
    const filtered: FilteredDateInterface = DateFilter.getFilteredDateObject(
      dateObject,
      lang,
    );
    const d: number = filtered.d as number;
    let m: number = filtered.m as number;
    m = m - 1; // m -= 1;

    for (const season in DateFilter.seasonsPeriodicMap[hemisphere]) {
      const seasonObj = (DateFilter.seasonsPeriodicMap as any)[hemisphere][
        season
      ];
      if (
        m >= seasonObj.start.month &&
        m <= seasonObj.end.month &&
        d <= seasonObj.end.day
      ) {
        seasonData.icon = (DateFilter.seasonsMap as any)[lang][season].icon;
        seasonData.label = (DateFilter.seasonsMap as any)[lang][season].label;
        break;
      } else if ([11, 0, 1].indexOf(+m) !== -1) {
        if (d <= seasonObj.end.day) {
          seasonData.icon =
            hemisphere === 'north'
              ? (DateFilter.seasonsMap as any)[lang]['winter'].icon
              : (DateFilter.seasonsMap as any)[lang]['summer'].icon;
          seasonData.label =
            hemisphere === 'north'
              ? (DateFilter.seasonsMap as any)[lang]['winter'].label
              : (DateFilter.seasonsMap as any)[lang]['summer'].label;
          break;
        }
      }
    }
    return seasonData;
  },

  zeroPrefix: (numValue: number) => {
    return numValue < 10 ? `0${numValue}` : `${numValue}`;
  },

  getFilteredDateObject: (
    dateObject: Date,
    lang = 'de',
  ): FilteredDateInterface => {
    const dayOfWeek = dateObject.getDay();
    return {
      y: dateObject.getFullYear(),
      m: DateFilter.month(dateObject.getMonth() + 1),
      d: DateFilter.date(dateObject.getDate()),
      h: DateFilter.hours(dateObject.getHours()),
      i: DateFilter.minutes(dateObject.getMinutes()),
      s: DateFilter.seconds(dateObject.getSeconds()),
      wd: (DateFilter.dayMap as any)[lang][`${dayOfWeek}`].ab,
      ms: (DateFilter.monthMap as any)[lang][`${dateObject.getMonth()}`].ab,
    };
  },
};

const getDefaultToolTipConfig = (toolTipText: string): any => {
  return {
    content: toolTipText,
    placement: 'bottom',
    arrow: true,
    animation: 'rubberBand',
    theme: 'light',
    allowHTML: true,
    delay: 250, // ms
    duration: 100,
    // followCursor: 'vertical',
  };
};

const playAudio = async (audioFileURI: string): Promise<HTMLAudioElement> => {
  const audio: any = new window.Audio();
  audio.src = audioFileURI;
  await audio.play();
  return audio;
};

const filterAnimationClasses = (domElement: HTMLElement): HTMLElement => {
  const classLists: DOMTokenList = domElement.classList;
  classLists.forEach((value: string, key: number, parent: DOMTokenList) => {
    if (/nt-comp-animate__/gi.test(value)) {
      domElement.classList.remove(value);
    }
  });
  domElement.classList.remove(
    'nt-comp-animate__fadeSlideInNotificationWrapper',
    'notification-component-animate',
    'nt-comp-animate__heartBeat',
    'nt-comp-animate__fadeLeftSlideNotificationWrapper',
    'nt-comp-animate__bounceOffNotificationWrapper',
    'nt-comp-animate__*',
  );
  return domElement;
};

export {
  ucFirst,
  ucWords,
  isLowerCase,
  isUpperCase,
  camelFy,
  snakeFy,
  playAudio,
  DateFilter,
  filterAnimationClasses,
  getDefaultToolTipConfig,
  dispatchCustomEvent,
  generateQuadRandomHash,
};
