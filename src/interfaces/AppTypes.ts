export type MessageType = 'info' | 'error' | 'warning';
export type MessageStatus = 'read' | 'unread';
export type NotificationOutputMedium = 'Browser' | 'Console' | 'Native Alert';
export type NotificationPreferenceType =
  | 'showAllNotifications'
  | 'showOnlyInfoNotifications'
  | 'showOnlyWarnNotifications'
  | 'showOnlyErrorNotifications';
