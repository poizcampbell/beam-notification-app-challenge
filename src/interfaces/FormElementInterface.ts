interface FormElementInterface {
  inputType: string;
  inputValue: string;
  inputName: string;
  label?: string;
  labelClass?: string;
  inputClass?: string;
  inputID?: string;
  labelID?: string;
  min?: string;
  max?: string;
  step?: string;
  placeholder?: string;
  options?: {[choiceOption: string]: string};
  dataAttributes?: {[dataAttribute: string]: string};
}

export {FormElementInterface};
