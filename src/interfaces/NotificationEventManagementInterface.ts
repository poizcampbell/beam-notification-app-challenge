import {MessageType} from '@src/interfaces/AppTypes';

interface NotificationEventManagementInterface {
  slot: HTMLElement;
  type: MessageType;
}
export {NotificationEventManagementInterface as NEMI};
