import {MessageType} from './AppTypes';

export interface MessageInterface {
  id: string | number;
  type: MessageType;
  body: string;
  date: Date;
  title: string;
  status?: string;
  duration?: number;
}

export interface MessageOperationsInterface {
  markMessageAsRead(msgID: string | number): void;
  markMessageAsUnRead(msgID: string | number): void;
  deleteMessage(msgID: string | number): void;
  renderMessage(msgID: string | number): void;
  tagMessage(msgID: string | number): void;
  positionMessage(msgID: string | number): void; // BETTER DO THIS WITHIN THE CONTEXT OF THE FILE INSTANTIATING THE CONCRETE CLASS.
  fetchMessageByID(msgID: string | number): MessageInterface | null;
  fetchUnreadMessages(msgID: string | number): MessageInterface[];
}
