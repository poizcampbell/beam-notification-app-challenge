import {devServerConfig} from './config';

export default {
  devtool: 'cheap-module-source-map',
  plugins: [],
  devServer: devServerConfig,
};
