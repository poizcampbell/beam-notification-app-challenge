# BEAM CODING CHALLENGE: A Minimal, Frontend-only Notification-System App.

This simple `No-App` App is built in `TypeScript: 4.0.3`.
For ease of Development, the Module is bundled using `Webpack: 5.24.2`
which ships with a `Dev. Sever` - ideal for Development.

This App. outputs a Notification Message to the `Output-Stream` saved in the Configuration.
The `Output Stream` could be any one of the following 3 Options:
`Browser`, `Console` and `Native Browser Alert`. The App. starts out by exposing `3 Circles` on
the Screen vis:

1. `Top-Left`: Green Circle -- **INFO TRIGGER**
2. `Bottom-Left`: Orange Circle -- **WARN TRIGGER**
3. `Bottom-Right`: Red Circle -- **ERROR TRIGGER**
4. `Top-Right`: Gear Icon -- **To toggle Settings Panel**

Click on the Gear to toggle open or close the Settings Panel.
Click on any of the Color-Coded Circles to start triggering Notifications.

## IMPORTANT:

There was not so much Time to fully implement all the intended Functionality and thus
certain configurations would have no effect on the App. For example: `Maximum Notifications per Session`
was not implemented as well as: `Notification Life-Span`. Additionally, the `Coverage` for the `Tests` is
very minimal, however the Code-base is relatively Testable. One `GOTCHA`, though:
_You may need to use `Qualified Pathnames` for `imports`
in every component, otherwise the Tests might not run appropriately_

## External Libraries used in the App.

Other than `WebPack` and the needed `Loaders` alongside `dependencies` for local development like: `ES-Lint`,
`Babel`, etc + Test Suites like `Jest`, `Mocha`... this App. is independent of any Library and should also run outside `Webpack` Environment... however,
`Tippy` was used as an extra Dependency specifically for showing `Tool-Tips`.
Equally, no external `CSS Framework` was used: All `CSS Styles` were `Custom crafted` in `SCSS` and compiled to
`CSS` using `appropriate loader`: thanks again to `Webpack`. `Font-Awesome` was also used - merely as Provider for `Icon-Fonts`.

## ASSETS

Sound (in `MP3` and `M4A` Formats) was used to further enhance UX. Simple Tones sampled from `Apple Sound-Effects` Collection.
Font-Awesome was used to add a Few Icons like the `Checkmark`, `Save` and `Gear` Icons. No Video or Image Material appears in this
Project. This Project does not interact with an external API or Script. Down or Upload Functionality is not part of this Project.

## TECH-STACK:

1. TYPESCRIPT.
2. HTML5.
3. SCSS.
4. JEST.
5. ES6.
6. BROWSER STORAGE (STORAGE API).

# INSTALLATION:

After cloning the `Repo`, `cd` into the folder containing the cloned repo. Then run the following Command(s):

```sh
# TO CLONE THE REPO, JUST RUN:
# git clone https://poizcampbell@bitbucket.org/poizcampbell/beam-notification-app-challenge.git

# THEN CD INTO THE PROJECT DIRECTORY:
# cd $PATH/TO/beam-notification-app-challenge

# INSTALL DEPENDENCIES:
yarn install # INSTALLS DEPENDENCIES FROM package.json FILE.

  # IF `yarn` DOESN'T EXIST ON MACHINE, YOU COULD ALTERNATIVELY RUN `npm install`
  # I'D STICK WITH YARN...

# START APP: VIEW IN BROWSER AT: http://0.0.0.0:8080
yarn start      # COMPILES + STARTS APP VIA DEV. SERVER. RUNS ON: http://0.0.0.0:8080

# BUILD APP FOR DEPLOYMENT...
yarn build      # BUILDS & BUNDLES THE APP. FOR USE IN PRODUCTION

# RUN TESTS USING `jest`
yarn test       # RUNS ALL UNIT-TESTS ( USES `jest`)

# RUN COVERAGE...
yarn coverage   # RUNS COVERAGE

```

## TESTS:

Please refer to the last 2 Commands in the `Code-Block` above. It is important to
note here that due to Time-Constraints, `Coverage` was really poor - meaning
that though the `Codebase` is expected to be testable, not many were tested
and also there might be breakages.... Expect also to find possible `Bugs`. As already
mentioned: this was put together really fast and with little time... so, some things
might have been possibly overlooked...

## SCREENSHOTS

![Desktop Screenshot: #1](https://bitbucket.org/poizcampbell/beam-notification-app-challenge/raw/cf9203786c09b8ebf1c7f5275af33134ee32306d/beam-test--setting-desktop-01.png)
![Desktop Screenshot: #2](https://bitbucket.org/poizcampbell/beam-notification-app-challenge/raw/cf9203786c09b8ebf1c7f5275af33134ee32306d/beam-test--notifications-desktop-01.png)
![Mobile Screenshot: #1](https://bitbucket.org/poizcampbell/beam-notification-app-challenge/raw/cf9203786c09b8ebf1c7f5275af33134ee32306d/beam-test--setting-mobile-01.png)
![Mobile Screenshot: #2](https://bitbucket.org/poizcampbell/beam-notification-app-challenge/raw/cf9203786c09b8ebf1c7f5275af33134ee32306d/beam-test--notifications-mobile-01.png)
![Console Screenshot: #1](https://bitbucket.org/poizcampbell/beam-notification-app-challenge/raw/c3a4eeec2541e10598dd7ed5dc702713aa55a61b/beam-test--setting-console-01.png)
![Native Alert Screenshot: #1](https://bitbucket.org/poizcampbell/beam-notification-app-challenge/raw/c3a4eeec2541e10598dd7ed5dc702713aa55a61b/beam-test--setting-alert-01.png)
